//https://codepen.io/eskensberg/pen/rNNNvNx

//todo 

// 1) play news
// 2) play snake game
// 3) screen saver
// 4) change font
// 5) teach coding
// 6) preset texts using url

var windowResized = false;

var columnWidth = 17;
var rowHeight = 15;

const isItTablet = isTablet();
var mobile = isMobile(); // broken

console.log('is tablet:' + isItTablet)
console.log('is mobile:' + mobile)

var columnCount = 100;
var rowCount = 100;
var counter = 100;
var animationsPlayed = 0;
var menuDisplayed = false;
var menuRevealFontSize = 28;
var menuCloseFontSize = 20;

var topLeftMenu = `#cell-3-${columnCount - 3}`;
var topRightMenu = `#cell-3-${columnCount - 2}`;
var bottomLeftMenu = `#cell-4-${columnCount - 3}`;
var bottomRightMenu = `#cell-4-${columnCount - 2}`;

function MenuCellOverlap(row, column) {
  if ((row === 2 || row === 3)
    && (column === columnCount - 2 || column === columnCount - 3)) {
    return true;
  }
  return false;
}



function renderMenu() {

  $(topLeftMenu).text('M').addClass('matrix-menu').css("font-size", `${menuRevealFontSize}px`);
  $(topRightMenu).text('E').addClass('matrix-menu').css("font-size", `${menuRevealFontSize}px`);
  $(bottomLeftMenu).text('N').addClass('matrix-menu').css("font-size", `${menuRevealFontSize}px`);
  $(bottomRightMenu).text('U').addClass('matrix-menu').css("font-size", `${menuRevealFontSize}px`);

  $('.matrix-menu').click(function (event) {
    event.preventDefault();

    if (menuDisplayed) {
      menuDisplayed = false;

      $(topLeftMenu).text('M').css("font-size", `${menuRevealFontSize}px`);
      $(topRightMenu).text('E').css("font-size", `${menuRevealFontSize}px`);
      $(bottomLeftMenu).text('N').css("font-size", `${menuRevealFontSize}px`);
      $(bottomRightMenu).text('U').css("font-size", `${menuRevealFontSize}px`);

    } else {
      menuDisplayed = true;

      $(topLeftMenu).text('//').css("font-size", `${menuCloseFontSize}px`);;
      $(topRightMenu).text('\\\\').css("font-size", `${menuCloseFontSize}px`);;
      $(bottomLeftMenu).text('\\\\').css("font-size", `${menuCloseFontSize}px`);;
      $(bottomRightMenu).text('//').css("font-size", `${menuCloseFontSize}px`);;

    }
  });
}

async function renderGrid() {

  columnCount = Math.floor(window.innerWidth / columnWidth); // depending on screen max over 2560
  rowCount = Math.floor(window.innerHeight / rowHeight); // depending on screen

  topLeftMenu = `#cell-3-${columnCount - 3}`;
  topRightMenu = `#cell-3-${columnCount - 2}`;
  bottomLeftMenu = `#cell-4-${columnCount - 3}`;
  bottomRightMenu = `#cell-4-${columnCount - 2}`;

  console.log('columnCount: ' + columnCount);
  console.log('rowCount: ' + rowCount);

  for (var row = 1; row <= rowCount; row++) {

    for (var collumn = 1; collumn <= columnCount; collumn++) {

      var item = `<div id="cell-${row}-${collumn}" class="grid-item"></div>`;

      $('.grid-container').append(item);
    }
  }

  $(".grid-item").click(function (event) {
    event.preventDefault();
    console.log($(this).attr('id'));
  });

  $('.grid-container').css("grid-template-columns", `repeat(${columnCount}, 1fr)`);
  $('.grid-container').css("grid-template-rows", `repeat(${rowCount}, 1fr)`);
  $('.grid-item').css("height", '15px');
  $('.grid-item').css("width", '14px');
  $('.grid-item').css("font-size", '18px');

  renderMenu();

}

renderGrid();

var matrix = [];


async function PushIntoMatrix(cell) {

  var currentValue = matrix[cell.counter];
  if (currentValue !== undefined) {
    currentValue.push(cell)
  } else {
    currentValue = [{
      'counter': cell.counter,
      'collumn': cell.collumn,
      'row': cell.row,
      'class': cell.class,
      'text': cell.text,
      'attr': cell.attr === undefined ? null : cell.attr,
    }];
  }

  matrix[cell.counter] = currentValue;
}

// var textTheMatrixHasYou = "The Matrix has you..."
// var stumblingTextLocation = {};

async function PlayLineTextInAMatrix(options) {

  var lastCount = matrix.length === 0 ? 0 : matrix.length + 60; // 60*50ms = 3seconds wait

  var stumblingTextLocation = {
    'text': options.text,
    'leftColumns': Math.floor((columnCount - options.text.length) / 2),
    'rightColumns': Math.floor(columnCount - ((columnCount - options.text.length) / 2)),
    'bottomRows': Math.floor(rowCount / 2),
  };

  for (var rainColumn = 1; rainColumn <= columnCount; rainColumn++) {

    var randomStart = Math.floor(Math.random() * 50)
    var randomOffset = Math.floor(Math.random() * rowCount * 0.3)
    var randomLength = Math.floor(Math.random() * rowCount * 0.25)

    for (var count = 1; count <= rowCount - randomLength; count++) {
      if (count < rowCount + 1) {

        var row = count + randomOffset;

        if (row === stumblingTextLocation.bottomRows) {

          if (rainColumn > stumblingTextLocation.leftColumns
            && rainColumn <= stumblingTextLocation.rightColumns) {

            await PushIntoMatrix({
              'counter': count + randomStart + lastCount,
              'collumn': rainColumn,
              'row': count + randomOffset,
              'class': "grid-item-rain-text-1",
              'attr': stumblingTextLocation.text[rainColumn - stumblingTextLocation.leftColumns - 1],
              'text': randomKana()
            });

          } else {
            await PushIntoMatrix({
              'counter': count + randomStart + lastCount,
              'collumn': rainColumn,
              'row': row,
              'class': "grid-item-rain-1",
              'text': randomKana()
            });
          }
        }
      }
    }
    counter = 0;
  }
}

var animations = [
  function () { PlayLineTextInAMatrix({ text: "code by: Elvis Skensberg" }); },
  function () { PlayLineTextInAMatrix({ text: "The Matrix has you..." }); },
  function () { PlayLineTextInAMatrix({ text: "The Matrix has me too..." }); }
];

function revealMatrix() {

  if (counter < 0) return;

  if (windowResized) {
    console.log('windowResized!')
    renderGrid();
  }

  var element = matrix[counter];

  if (element !== undefined && element.length > 0) {

    element.forEach(cell => {

      if (cell.attr === null) {
        $(`#cell-${cell.row}-${cell.collumn}`)
          .addClass(`${cell.class}-${counter + 20}`)
          .text(cell.text)
          .css({ 'color': 'white' })
      } else {
        $(`#cell-${cell.row}-${cell.collumn}`)
          .addClass(`${cell.class}-${counter + 15}`)
          .text(cell.text)
          .css({ 'color': 'white' })
          .attr("textAttr", cell.attr);;
      }
    });

  }


  $(`.grid-item-rain-text-1-${counter}`)
    .removeClass(`grid-item-rain-text-1-${counter}`)
    .addClass(`grid-item-rain-text-2-${counter + 15}`)
    .css({ 'color': 'red' });

  $(`.grid-item-rain-text-2-${counter}`)
    .removeClass(`grid-item-rain-text-2-${counter}`)
    .addClass(`grid-item-rain-text-3-${counter + 15}`)
    .css({ 'color': 'green' }).text(randomKana());

  $(`.grid-item-rain-text-3-${counter}`)
    .removeClass(`grid-item-rain-text-3-${counter}`)
    .addClass(`grid-item-rain-text-4-${counter + 15}`)
    .css({ 'color': 'greenyellow' });

  $(`.grid-item-rain-text-4-${counter}`)
    .removeClass(`grid-item-rain-text-4-${counter}`)
    .addClass(`grid-item-rain-text-5-${counter + 10}`)
    .css({ 'color': 'red' });

  var finalTextAnimation = $(`.grid-item-rain-text-5-${counter}`);

  if (finalTextAnimation.length > 0) { // need foreach loop

    finalTextAnimation.each(function (i, cell) {
      var textValue = $(cell).attr("textattr");
      $(cell).text(textValue).removeClass(`grid-item-rain-text-5-${counter}`)
        .removeAttr("textattr")
        .css({ 'color': 'white' });
    });
  }



  $(`.grid-item-rain-1-${counter}`)
    .removeClass(`grid-item-rain-1-${counter}`)
    .addClass(`grid-item-rain-2-${counter + 20}`)
    .css({ 'color': 'red' });

  $(`.grid-item-rain-2-${counter}`)
    .removeClass(`grid-item-rain-2-${counter}`)
    .addClass(`grid-item-rain-3-${counter + 20}`)
    .css({ 'color': 'green' });

  $(`.grid-item-rain-3-${counter}`)
    .removeClass(`grid-item-rain-3-${counter}`)
    .css({ 'color': 'black' });




  counter++;

  if (counter > matrix.length + 100) { // end of matrix

    matrix = [];
    counter = -1;
    if (animations.length > 0) {
      animations.shift()();
    }
  }
}


// RainFor(8 rows, 2 seconds, from 2,2 etc)


// antoni teamlead
// rpa robotic automation apps

// for (var rainColumn = 1; rainColumn <= columnCount; rainColumn++) {
//   var randomStart = Math.floor(Math.random() * 50)
//   var randomOffset = Math.floor(Math.random() * rowCount * 0.3)
//   var randomLength = Math.floor(Math.random() * rowCount * 0.25)
//   // console.log(`column: ${rainColumn} and random: ${random}`)

//   for (var count = 1; count <= rowCount - randomLength; count++) {
//     if (count < rowCount + 1) {

//       var row = count + randomOffset;

//       if (row < stumblingTextLocation.bottomRows) {

//         //  PushIntoMatrix({
//         //   'counter': count + randomStart,
//         //   'collumn': rainColumn,
//         //   'row': row,
//         //   'class': "grid-item-rain-1",
//         //   'text': randomKana()
//         // });

//       } else if (row === stumblingTextLocation.bottomRows) {

//         console.log(`rainColumn: ${rainColumn}`)
//         console.log(stumblingTextLocation)
//         console.log(`rainColumn > left: ${rainColumn > stumblingTextLocation.leftColumns}`)
//         console.log(`rainColumn < right: ${rainColumn < stumblingTextLocation.rightColumns}`)

//         if (rainColumn > stumblingTextLocation.leftColumns
//           && rainColumn < stumblingTextLocation.rightColumns) {

//            PushIntoMatrix({
//             counter: count + randomStart,
//             collumn: rainColumn,
//             row: count + randomOffset,
//             class: "grid-item-rain-text",
//             text: stumblingTextLocation.text[rainColumn - stumblingTextLocation.leftColumns - 1]  
//           });


//         } else {
//           matrix = PushIntoMatrix({
//             'counter': count + randomStart,
//             'collumn': rainColumn,
//             'row': row,
//             'class': "grid-item-rain-1",
//             'text': randomKana()
//           });
//         }
//       } else if (rainColumn <= stumblingTextLocation.leftColumns
//         || rainColumn >= stumblingTextLocation.rightColumns) {

//         matrix = PushIntoMatrix({
//           counter: count + randomStart,
//           collumn: rainColumn,
//           row: count + randomOffset,
//           class: "grid-item-rain-1",
//           text: randomKana()
//         });

//       }


//     }
//   }
// }
// random meme from matrix
// console.log(matrix)

// clearInterval(revealMatrixInterval);
var revealMatrixInterval = setInterval(revealMatrix, 50);

